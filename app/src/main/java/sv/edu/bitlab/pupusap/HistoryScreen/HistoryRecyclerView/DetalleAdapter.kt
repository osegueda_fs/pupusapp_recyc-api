package sv.edu.bitlab.pupusap.HistoryScreen.HistoryRecyclerView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sv.edu.bitlab.pupusap.R

class DetalleAdapter(val arraypupas: ArrayList<Int>, val listener: DetalleViewHolder.RowListener):RecyclerView.Adapter<DetalleViewHolder>() {
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetalleViewHolder {
    val view: View = LayoutInflater.from(parent.context).inflate(R.layout.detalle_row, parent, false)
    return DetalleViewHolder(view,listener)
  }

  override fun getItemCount(): Int {
    return arraypupas.size
  }

  override fun onBindViewHolder(holder: DetalleViewHolder, position: Int) {
    holder.onBindData(arraypupas[position], position)
  }


}