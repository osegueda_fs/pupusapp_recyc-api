package sv.edu.bitlab.pupusap.HistoryScreen
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.Context
import android.content.res.Configuration
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import kotlinx.android.synthetic.main.activity_main.*
import sv.edu.bitlab.pupusap.Models.Orden
import sv.edu.bitlab.pupusap.Models.OrdenPupusas
import sv.edu.bitlab.pupusap.Models.Relleno
import sv.edu.bitlab.pupusap.Models.TakenOrden
import sv.edu.bitlab.pupusap.Network.ApiService
import sv.edu.bitlab.pupusap.OrdenDetalleFragment
import sv.edu.bitlab.pupusap.R
public const val VARIABLE = 2
class HistoryActivity : AppCompatActivity(),
  HistoryListFragment.HistoryListFragmentListener,
  OrdenDetalleFragment.OrdenDetalleFragmentListener {
  private var page = 0

  private lateinit var ordenes: ArrayList<Orden>

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

   /*ordenes = (if(savedInstanceState == null)
     TakenOrden.randomOrders()
   else
     savedInstanceState.getParcelableArrayList(HISTORIAL_DE_ORDENES)!!) as ArrayList<Orden>*/

    //ordenes = savedInstanceState!!.getParcelableArrayList(HISTORIAL_DE_ORDENES)!!

    if(resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
      setContentView(R.layout.activity_history_landscape)
      initLandsacpe()
    } else{
      setContentView(R.layout.activity_history_portrait)
      initPortrait()
    }
  }//EndOnCreate

  fun initLandsacpe(){
    loadListFragment(R.id.listContainer)
  }

  fun initPortrait(){
    loadListFragment(R.id.fragmentContainer)
  }

  fun loadListFragment(containerID: Int) {
    //loadingContainer.visibility = View.VISIBLE
        ApiService.create().getOrdenes().enqueue(object : Callback<ArrayList<Orden>> {
            override fun onFailure(call: Call<ArrayList<Orden>>, t: Throwable) {
                loadingContainer.visibility = View.GONE
                //Log.e("PUPUSA", "ERROR => ", t)
                AlertDialog.Builder(getContent())
                    .setTitle("ERROR")
                    .setMessage("Error con el servidor lo sentimos")
                    .setNeutralButton("ok", null)
                    .create()
                    .show()
            }

            override fun onResponse(
                call: Call<ArrayList<Orden>>,
                response: Response<ArrayList<Orden>>
            ) {

              val ordenes = response.body()!!
              val fragment = HistoryListFragment.newInstance(ordenes)
              val builder = supportFragmentManager
                .beginTransaction()
                .replace(containerID, fragment, LIST_FRAGMENT_TAG)
                .addToBackStack(LIST_FRAGMENT_TAG)
              builder.commitAllowingStateLoss()

              println(ordenes.get(0).arroz.get(0).total)
              
            }
        })

  }

  fun loadDetailFragment(containerID: Int, orden: Orden) {
    val fragment = OrdenDetalleFragment.newInstance(orden)
    val builder = supportFragmentManager
      .beginTransaction()
      .replace(containerID, fragment, DETAIL_FRAGMENT_TAG)
      .addToBackStack(DETAIL_FRAGMENT_TAG)
  }

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)
    outState.putParcelableArrayList(HISTORIAL_DE_ORDENES, ordenes)
  }

  override fun onItemClicked(position: Int) {
    val orden = ordenes[position]
    if(resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
      loadDetailFragment(R.id.detailContainer, orden)
    } else {
      loadDetailFragment(R.id.fragmentContainer, orden)
      page++
    }
  }

  override fun onFragmentInteraction(uri: Uri) {
  }

  companion object{
    const val LIST_FRAGMENT_TAG = "ORDENES_HISTORY"
    const val DETAIL_FRAGMENT_TAG = "DETAIL_FRAGMENT_TAG"
    const val HISTORIAL_DE_ORDENES = "HISTORIAL_DE_ORDENES"
  }

  fun getContent(): Context {
    return this
  }

  override fun onBackPressed() {

    if(page > 0){
      page--
      super.onBackPressed()
      Animatoo.animateSwipeRight(this)
    } else {
      finish()
    }

  }


}
