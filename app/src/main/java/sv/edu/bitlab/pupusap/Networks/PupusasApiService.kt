package sv.edu.bitlab.pupusap.Network

import retrofit2.Call
import retrofit2.http.*
import sv.edu.bitlab.pupusap.Models.Orden
import sv.edu.bitlab.pupusap.Models.Relleno
import sv.edu.bitlab.pupusap.Models.TakenOrden
import sv.edu.bitlab.pupusap.Models.RellenoWrapper

interface PupusasApiService {
  @GET("rellenos/")
  fun getRellenos() : Call<List<Relleno>>

  @GET("ordens/")
  fun getOrdenes() : Call<ArrayList<Orden>>

  @POST("ordens/")
  fun submitOrden(@Body params: Orden, @Path("id") id:Int) : Call<Orden>

}