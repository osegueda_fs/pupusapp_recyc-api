package sv.edu.bitlab.pupusap

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
/*
import sv.edu.bitlab.pupusap.DetalleOrdeActivity.Companion.CONTADOR_ARROZ
import sv.edu.bitlab.pupusap.DetalleOrdeActivity.Companion.CONTADOR_MAIZ
*/
import sv.edu.bitlab.pupusap.HistoryScreen.HistoryActivity
import sv.edu.bitlab.pupusap.HistoryScreen.HistoryRecyclerView.ButtonsAdapter
import sv.edu.bitlab.pupusap.HistoryScreen.HistoryRecyclerView.ButtonsViewHolder
import sv.edu.bitlab.pupusap.Models.Relleno
import sv.edu.bitlab.pupusap.Models.TakenOrden
import sv.edu.bitlab.pupusap.Network.ApiService

class MainActivity : AppCompatActivity(), ButtonsViewHolder.RellenoListener {
    override fun onCounterBtnClick(relleno: String, masa: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onMaizClick(relleno: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onArrozClick(relleno: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    //val orden = TakenOrden()

    val pupusaStringResources = hashMapOf(
        Queso to R.string.pupusa_queso,
        Frijoles to R.string.frijol_con_queso,
        Revueltas to R.string.revueltas,
        Jalapeño to R.string.jalapeno,
        Ajo to R.string.ajo
    )



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val orden = TakenOrden()
        val loadingContainer = findViewById<View>(R.id.loadingContainer)
        val rellenosList = findViewById<RecyclerView>(R.id.recycler)
        rellenosList.layoutManager = LinearLayoutManager(this)
        rellenosList.adapter = ButtonsAdapter(
            arrayListOf<Relleno>(),
            listener = this
        )
        loadingContainer.visibility = View.VISIBLE

        ApiService.create().getRellenos().enqueue(object : Callback<List<Relleno>> {
            override fun onFailure(call: Call<List<Relleno>>, t: Throwable) {
                loadingContainer.visibility = View.GONE
                Log.e("PUPUSA", "ERROR => ", t)
                AlertDialog.Builder(getContent())
                    .setTitle("ERROR")
                    .setMessage("Error con el servidor lo sentimos")
                    .setNeutralButton("ok", null)
                    .create()
                    .show()
            }

            override fun onResponse(
                call: Call<List<Relleno>>,
                response: Response<List<Relleno>>
            ) {
                loadingContainer.visibility = View.GONE
                val rellenos = response.body()!!
                val adapter = rellenosList.adapter as ButtonsAdapter
                adapter.rellenos = rellenos
                adapter.notifyDataSetChanged()
            }
        })
    } //endOncreate
    fun getContent(): Context {
        return this
    }

    //inflamos el submenu con la opcion del historial
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_item, menu)
        return super.onCreateOptionsMenu(menu)
    }

    //Agregamos el evento click a la opcion del menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items
        when (item.itemId) {
            R.id.menu_historial -> {
                val intent = Intent(this, HistoryActivity::class.java)
                this.startActivity(intent)
                Animatoo.animateSwipeLeft(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun confirmarOrden() {

    }



    /*override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)

    }*/

    /*companion object{
        const val MAIZ = "MAIZ"
        const val ARROZ = "ARROZ"
    }*/

}







