package sv.edu.bitlab.pupusap.HistoryScreen.HistoryRecyclerView

import android.view.View
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import sv.edu.bitlab.pupusap.Models.Relleno
import sv.edu.bitlab.pupusap.R

class ButtonsViewHolder(itemView: View, val listener: RellenoListener):RecyclerView.ViewHolder(itemView) {

    var botonMaiz = hashMapOf<String, Button>()
   var botonArroz = hashMapOf<String, Button>()

  fun onBindData(rellenos: Relleno){
    var button1: Button = itemView.findViewById(R.id.botonIzquierdaMaiz)
    var button2:Button = itemView.findViewById(R.id.botonDerechaArroz)
    button1.text=rellenos.nombre
    button2.text=rellenos.nombre


    //button1.setOnClickListener{listener.onClickMaiz(maiz, position)}

   /* button1.setOnClickListener{
      listener.onClickCounter(botonMaiz)
      listener.onClickMaiz(maiz, botonMaiz)
    }

    button2.setOnClickListener{
      listener.onClickCounter(botonArroz)
      listener.onClickArroz(arroz, botonArroz)
    }*/


  }
  interface RellenoListener{
    fun onCounterBtnClick(relleno: String, masa: String)
    fun onMaizClick(relleno: String)
    fun onArrozClick(relleno: String)

  }
}